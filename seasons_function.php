<?php
    function current_season() {
        // Locate the icons
        $banners = array(
            "spring" => array(
                "/wp-content/themes/holloway/assets/images/skylines/spring-1.png",
                "/wp-content/themes/holloway/assets/images/skylines/spring-2.png",
                "/wp-content/themes/holloway/assets/images/skylines/spring-3.png",
                "/wp-content/themes/holloway/assets/images/skylines/spring-4.png",
            ),
            "summer" => array(
                "/wp-content/themes/holloway/assets/images/skylines/summer-1.png",
                "/wp-content/themes/holloway/assets/images/skylines/summer-2.png",
                "/wp-content/themes/holloway/assets/images/skylines/summer-3.png",
                "/wp-content/themes/holloway/assets/images/skylines/summer-4.png",
            ),
            "autumn" => array(
                "/wp-content/themes/holloway/assets/images/skylines/autumn-1.png",
                "/wp-content/themes/holloway/assets/images/skylines/autumn-2.png",
                "/wp-content/themes/holloway/assets/images/skylines/autumn-3.png",
                "/wp-content/themes/holloway/assets/images/skylines/autumn-4.png",
            ),
            "winter" => array(
                "/wp-content/themes/holloway/assets/images/skylines/winter-1.png",
                "/wp-content/themes/holloway/assets/images/skylines/winter-2.png",
                "/wp-content/themes/holloway/assets/images/skylines/winter-3.png",
                "/wp-content/themes/holloway/assets/images/skylines/winter-4.png",
            )
        );

        // find day number of today
        $day = date("z");

        // start date for each season
        $spring = date("z", strtotime("March 21"));
        $summer = date("z", strtotime("June 21"));
        $autumn = date("z", strtotime("September 23"));

        // set season according to the start date
        if ($day < $spring) {
            $season = "winter";
        }
        elseif($day < $summer) {
            $season = "spring";
        }
        elseif($day < $autumn) {
            $season = "summer";
        }
        else {
            $season = "autumn";
        }

        // get a random value
        $image_path = array_rand($banners[$season], 1);
        // push that value back to the array
        $seasonBanner = $banners[$season][$image_path];
        // get the image
        echo $seasonBanner;
    }

    function current_season_quad() {
        // Locate the icons
        $banners = array(
            "spring" => array(
                "/wp-content/themes/holloway/assets/images/skylines/spring-1.png",
                "/wp-content/themes/holloway/assets/images/skylines/spring-2.png",
                "/wp-content/themes/holloway/assets/images/skylines/spring-3.png",
                "/wp-content/themes/holloway/assets/images/skylines/spring-4.png",
            ),
            "summer" => array(
                "/wp-content/themes/holloway/assets/images/skylines/summer-1.png",
                "/wp-content/themes/holloway/assets/images/skylines/summer-2.png",
                "/wp-content/themes/holloway/assets/images/skylines/summer-3.png",
                "/wp-content/themes/holloway/assets/images/skylines/summer-4.png",
            ),
            "autumn" => array(
                "/wp-content/themes/holloway/assets/images/skylines/autumn-1.png",
                "/wp-content/themes/holloway/assets/images/skylines/autumn-2.png",
                "/wp-content/themes/holloway/assets/images/skylines/autumn-3.png",
                "/wp-content/themes/holloway/assets/images/skylines/autumn-4.png",
            ),
            "winter" => array(
                "/wp-content/themes/holloway/assets/images/skylines/winter-1.png",
                "/wp-content/themes/holloway/assets/images/skylines/winter-2.png",
                "/wp-content/themes/holloway/assets/images/skylines/winter-3.png",
                "/wp-content/themes/holloway/assets/images/skylines/winter-4.png",
            )
        );

        // find day number of today
        $day = date("z");

        // start date for each season
        $spring = date("z", strtotime("March 21"));
        $summer = date("z", strtotime("June 21"));
        $autumn = date("z", strtotime("September 23"));

        // set season according to the start date
        if ($day < $spring) {
            $season = "winter";
        }
        elseif($day < $summer) {
            $season = "spring";
        }
        elseif($day < $autumn) {
            $season = "summer";
        }
        else {
            $season = "autumn";
        }
        ?>

        <div class="owl-carousel owl-carousel-season">
            <?php
                for ($i = 0; $i < count($banners[$season]); $i++)  {
                    echo '<div>
                            <img src="'.$banners[$season][$i].'" />
                        </div>
                    ';
                }
            ?>
        </div>

<?php }

    function current_season_mixed() {
        // Locate the icons
        $banners = array(
            // spring
            "/wp-content/themes/holloway/assets/images/skylines/spring-1.png",
            "/wp-content/themes/holloway/assets/images/skylines/spring-2.png",
            "/wp-content/themes/holloway/assets/images/skylines/spring-3.png",
            "/wp-content/themes/holloway/assets/images/skylines/spring-4.png",
            // summer
            "/wp-content/themes/holloway/assets/images/skylines/summer-1.png",
            "/wp-content/themes/holloway/assets/images/skylines/summer-2.png",
            "/wp-content/themes/holloway/assets/images/skylines/summer-3.png",
            "/wp-content/themes/holloway/assets/images/skylines/summer-4.png",
            // autumn
            "/wp-content/themes/holloway/assets/images/skylines/autumn-1.png",
            "/wp-content/themes/holloway/assets/images/skylines/autumn-2.png",
            "/wp-content/themes/holloway/assets/images/skylines/autumn-3.png",
            "/wp-content/themes/holloway/assets/images/skylines/autumn-4.png",
            // winter
            "/wp-content/themes/holloway/assets/images/skylines/winter-1.png",
            "/wp-content/themes/holloway/assets/images/skylines/winter-2.png",
            "/wp-content/themes/holloway/assets/images/skylines/winter-3.png",
            "/wp-content/themes/holloway/assets/images/skylines/winter-4.png",
        );

        shuffle($banners);

        //print_r($banners);
    ?>

        <div class="owl-carousel owl-carousel-season">
            <?php
                foreach ($banners as &$banner) {
                    echo '<div>
                            <img src="'.$banner.'" />
                        </div>
                    ';
                }
            ?>
        </div>
<?php } ?>
